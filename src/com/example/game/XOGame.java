package com.example.game;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;

public class XOGame extends Activity {
	/** Called when the activity is first created. */
	public Paint paint;
	public int player = 1;
	public int[][] value = new int[3][3];
	public int win = 0;
	public boolean havewin = false;
	public int count = 0;
	public boolean pause_flag = false;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		paint = new Paint();
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setRequestedOrientation(
				ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(new Panel(this));
	}

	class Panel extends SurfaceView 
				implements SurfaceHolder.Callback {
		private TutorialThread _thread;

		public Panel(Context context) {
			super(context);
			getHolder().addCallback(this);
			_thread = new TutorialThread(getHolder(), this);
			setFocusable(true);
		}

		public boolean onTouchEvent(MotionEvent event) {
			// TODO Auto-generated method stub
			if (!pause_flag) {
				float w = this.getWidth();
				float h = this.getHeight();
				int bw = (int) (w / 3);
				int bh = (int) (h / 3);

				int vx = (int) event.getX() / bw;
				int vy = (int) event.getY() / bh;
				if (value[vx][vy] == 0) {
					if (player == 1) {
						value[vx][vy] = 1;
						player = 2;
					} else {
						value[vx][vy] = 2;
						player = 1;
					}

					win = checkWinner();
					count++;
					if (win > 0 || count == 9) {
						pause_flag = true;
					}
					Log.v("point", "x:" + vx + "," 
								 + "y:" + vy);
					Log.v("count", "c : " + count);
					Log.v("win", "w:" + win);
				}
			} else {
				Log.v("clear", "w:" + win);
				for (int i = 0; i < 3; i++) {
					for (int j = 0; j < 3; j++) {
						value[i][j] = 0;
					}
				}
				player = 1;
				win = 0;
				count = 0;
				pause_flag = false;
			}
			return super.onTouchEvent(event);
		}

		public int checkWinner() {
			// check row
			for (int i = 0; i < 3; i++) {
				if (value[i][0] == value[i][1] 
				        && value[i][1] == value[i][2]
						&& value[i][0] != 0 
						&& value[i][1] != 0
						&& value[i][2] != 0) {
					Log.v("check", "row = " + i);
					
					// return number win
					return value[i][0]; 
				}
			}
			// check col
			for (int i = 0; i < 3; i++) {
				if (value[0][i] == value[1][i] 
				        && value[1][i] == value[2][i]
						&& value[0][i] != 0 
						&& value[1][i] != 0
						&& value[2][i] != 0) {
					Log.v("check", "col = " + i);
					
					// return number win
					return value[0][i]; 
				}
			}
			// check diagonal
			if (value[0][0] == value[1][1] 
			        && value[1][1] == value[2][2]
					&& value[0][0] != 0 
					&& value[1][1] != 0 
					&& value[2][2] != 0) {
				
				// return number win
				return value[0][0]; 

			} else if (value[0][2] == value[1][1] 
			        && value[1][1] == value[2][0]
					&& value[0][2] != 0 
					&& value[1][1] != 0 
					&& value[2][0] != 0) {
				return value[0][2];
			}
			return 0;
		}

		public void onDraw(Canvas canvas) {
			float w = this.getWidth();
			float h = this.getHeight();
			int bw = (int) (w / 3);
			int bh = (int) (h / 3);
			paint = new Paint();
			paint.setColor(Color.BLACK);
			canvas.drawRect(0, 0, w, h, paint);

			paint.setColor(Color.WHITE);
			paint.setAntiAlias(true);
			paint.setStrokeWidth(10);
			paint.setStrokeCap(Cap.ROUND);
			// draw table
			canvas.drawLine(bw, 5, bw, h - 5, paint);
			canvas.drawLine(2 * bw, 5, 
					2 * bw, h - 5, paint);

			canvas.drawLine(5, bh, w - 5, bh, paint);
			canvas.drawLine(5, 2 * bh, 
					w - 5, 2 * bh, paint);

			// draw x or o
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 3; j++) {
					switch (value[i][j]) {
					case 1:// plyer 1 draw x
						paint.setColor(Color.rgb(
								246, 107, 22));
						canvas.drawLine(i * bw + bw 
								/ 2 + bh 
								/ 2 - 20, j * bh + bh 
								/ 2 + bh / 2 - 20, i 
								* bw + bw / 2 - bh
								/ 2 + 20, j * bh + bh 
								/ 2 - bh / 2 + 20, 
								paint);
						canvas.drawLine(i * bw + bw 
								/ 2 + bh 
								/ 2 - 20, j * bh
								+ bh / 2 - bh 
								/ 2 + 20, i 
								* bw + bw / 2 - bh
								/ 2 + 20, j * bh + bh 
								/ 2 + bh / 2 - 20, 
								paint);
						break;
					case 2: // plyer 2 draw O
						paint.setColor(Color.rgb(54, 
								130, 242));
						canvas.drawCircle(i * bw + bw 
								/ 2, j * bh + bh / 2,
								(bh - 20) / 2, paint);
						paint.setColor(Color.BLACK);
						canvas.drawCircle(i * bw + bw 
								/ 2, j * bh + bh / 2,
								(bh - 40) / 2, paint);
						break;
					default:
					}
				}
			}

			if (pause_flag) {
				paint.setColor(Color.argb(180, 0, 0, 0));
				canvas.drawRect(0, 0, w, h, paint);
				Log.v("ox win ", "win" + win);
				String txt = "";
				if (win == 0) {
					txt = "No Win!";
				} else if (win == 1) {
					txt = "X Win!";
				} else if (win == 2) {
					txt = "O Win!";
				}
				paint.setColor(Color.MAGENTA);
				paint.setTextSize(100);
				paint.setTextAlign(Paint.Align.CENTER);
				canvas.drawText(txt, w / 2, 
						h / 2, paint);
				paint.setTextSize(20);
				canvas.drawText("Please touch on " +
						"screen to new game.", w / 2,
						h / 2 + 30, paint);
			}
		}

		public void surfaceChanged(SurfaceHolder holder, 
				int format, int width, int height) {
			// TODO Auto-generated method stub
		}

		public void surfaceCreated(SurfaceHolder holder) {
			_thread.setRunning(true);
			_thread.start();
		}

		public void surfaceDestroyed(
				SurfaceHolder holder) {
			boolean retry = true;
			_thread.setRunning(false);
			while (retry) {
				try {
					_thread.join();
					retry = false;
				} catch (InterruptedException e) {
					// we will try it again and again...
				}
			}
		}
	}

	class TutorialThread extends Thread {
		private SurfaceHolder _surfaceHolder;
		private Panel _panel;
		private boolean _run = false;

		public TutorialThread(SurfaceHolder surfaceHolder, 
				Panel panel) {
			_surfaceHolder = surfaceHolder;
			_panel = panel;
		}

		public void setRunning(boolean run) {
			_run = run;
		}

		public SurfaceHolder getSurfaceHolder() {
			return _surfaceHolder;
		}

		public void run() {
			Canvas c;
			while (_run) {
				c = null;
				try {
					c = _surfaceHolder.lockCanvas(null);
					synchronized (_surfaceHolder) {
						_panel.onDraw(c);
					}
				} finally {
					if (c != null) {
						_surfaceHolder.
						unlockCanvasAndPost(c);
					}
				}
			}
		}
	}
}
